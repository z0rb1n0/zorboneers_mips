#!/bin/bash -eu


# Removes the odd non-ASCII character that copy-pasting
# from the in-game editor seems to introduce.
#
# WARNING: does so repository-wide


declare -r -x PATH='/usr/local/bin:/usr/bin:/bin';


declare -r -x LANG='C'; # this avoids awk trying to resolve collation



	cd "${0%/*}" || exit 3;

		find -type f -iname '*.IC10' -print0 | \
		xargs --null --no-run-if-empty sed -i -r 's/[\x7f-\xff]+//g;' \
	;
