# Basic aircon & exchanger loop.
# Controls a diverter pump to the exchanger
# and a tapoff one to cool the environment

# The diverter to the exchanger is modulated
# by available headspace in the exchanger,
# the tapoff by difference of ambient to target
# pressure and pressure in the exchanger
# (to maintain a relatively constant flow)

define TARGET_PRESSURE 4000
# How many degrees of amb. temperature difference
# for the tap pump to be maxed out?
define PUMP_CEIL_OT 20.0
# How many 1atm liters/tick does "maxed out" mean?
define TAPOFF_CEIL 400


alias mem_target_temp d0
alias pipe_return_air d1
alias tank_exchanger d2
alias vpump_chill d3
alias vpump_tapoff d4

alias target_temp r0
alias return_temp r1
alias exch_pres r2
alias exch_temp r3
alias chill_pump_rate r4

alias needed_effect r5 # <0 cooling, >0 heating
alias exch_useful r6
alias tapoff_pump_rate r7


# lock & load
s vpump_chill Lock 1
s vpump_chill On 1
s vpump_tapoff Lock 1
s vpump_tapoff On 1

main:
yield

move chill_pump_rate 0
move tapoff_pump_rate 0

l exch_pres tank_exchanger Pressure
l exch_temp tank_exchanger Temperature
l target_temp mem_target_temp Setting
l return_temp pipe_return_air Temperature

# chill pump
sub chill_pump_rate TARGET_PRESSURE exch_pres
max chill_pump_rate 0 chill_pump_rate # to 0.0-1.0
div chill_pump_rate chill_pump_rate TARGET_PRESSURE
mul chill_pump_rate 100 chill_pump_rate

# tap pump. Determine if exchanger
# temperature takes us in the right direction
sub needed_effect target_temp return_temp
sub exch_useful exch_temp return_temp
mul exch_useful needed_effect exch_useful
blez exch_useful exchanger_useless

abs tapoff_pump_rate needed_effect
div tapoff_pump_rate tapoff_pump_rate PUMP_CEIL_OT
max tapoff_pump_rate tapoff_pump_rate 0.0
min tapoff_pump_rate 1.0 tapoff_pump_rate
# account for exchanger pressure
mul tapoff_pump_rate tapoff_pump_rate 101.325
div tapoff_pump_rate tapoff_pump_rate exch_pres
mul tapoff_pump_rate TAPOFF_CEIL tapoff_pump_rate

exchanger_useless:

s vpump_chill Setting chill_pump_rate
s vpump_tapoff Setting tapoff_pump_rate


j main
