# Determinses partial pressures in the returning mix and
# output line, and determines at which rates each of the gases
# should be replenished, and which of the 3
# breathable gas scrubbers should be bypassed/active
#
# No replenishment happens if the evacuation
# shutoff valve is closed
#
# Devices are controlled through 2 ICs used as busses
#
# The 3 topping pumps are modulated by passing
# their flow rate pct to a bus IC packed as
# groups of 3 digits (0-999 range)
#
# The scrubbers + their bypass valves are controlled
# by a bitmask sent to the bus, each flag indicating
# wheter or not a given gas should be scrubbed

# in order to maintain an Earth-like atmosphere.
# Controls 2 gas mixers directly, and
# uses another IC "bus" to activates co2/n2/o2
# filters or their respective bypass valves
# It does to by sending the bus a bitmask
# of what gas should be scrubbed.
#
# It is recommended to measure the current
# values right at the point the return loop
# enters the cooling & filtration system

# Also controls the dirty feed (from furnaces/hot atmo)
# into the exchanger
